const generateMessage = (username, text) =>{
    console.log(username);
    return {
        text,
        username,
        createdAt: new Date().getTime()
    }
}

module.exports = {
    generateMessage
}