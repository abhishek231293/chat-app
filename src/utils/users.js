class Users {

    constructor(){
        this.users = []
    }

    addUser({id, userid, username, room}){
        console.log(room);
        username = unescape(username).trim().toLowerCase(), 
        room = unescape(room).trim().toLowerCase()
       
        if(!username || !userid || !room){
            return {
                status: 400,
                message: 'Username and room are required!'
            }
        }

        const existingUser = this.users.find((user)=>{
            return ((user.room === room) && (user.userid === userid))
        })

        if(existingUser){
            return {
                status: 400,
                message: 'Multiple channel not allowed for same room!'
            }
        }

        this.users.push({id, userid, username, room})

        return {
                    status:200,
                    data: {id, userid, username, room},
                    message: 'Successfully joined room'
                }
    }

    removeUser(id){
        const index = this.users.findIndex((user)=>user.id === id)

        if(index !== -1){
            const user = this.users.splice(index, 1)[0]

            return {
                status:200,
                data: user,
                message: 'User removed successfully'
            }
        }else{
            return {
                status: 400,
                message: 'User not found!'
            }
        }
    }

    getUsersInRoom(room){
        const users = this.users.filter((user) => user.room === room)

        if(users){
            return {
                status:200,
                data: users,
                message: 'User list fetched successfully'
            }
        }else{
            return {
                status: 400,
                message: 'Room not found!'
            }
        }
    }

    getUser(id){
        const user = this.users.find((user)=> user.id === id)

        if(user){
            return {
                status:200,
                data: user,
                message: 'User fetched successfully'
            }
        }else{
            return {
                status: 400,
                message: 'User not found!'
            }
        }
    }

}

module.exports = Users