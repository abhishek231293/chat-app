const mongoose = require('mongoose')
const validator = require('validator')
require('dotenv').config()
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'User name required'],
        trim: true
    },
    email:{
        type: String,
        required: [true, 'Email is required'],
        unique:[true, 'Email already registered'],
        lowercase:true,
        trim: true,
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Invalid Email')
            }
        }
    },
    password:{
        type: String,
        required:[true, 'Password is required'],
        trim: true,
        minlength:[8, 'Min 8 character required'],
        validate(value){
            if(value.toLowerCase().includes('password')){
                throw new Error('Password should not contain password')
            }
        }
    },
    tokens: [{
        token:{
            type: String,
            require: true
        }
    }],
    profile_pic:{
        type: Buffer
    }
}, {
    timestamps:true
})

userSchema.methods.toJSON = function(){
    const user = this
    const userObj = user.toObject()

    delete userObj.password
    delete userObj.tokens
    delete userObj.profile_pic

    return userObj
}

userSchema.pre('save', async function(next){
    const user = this

    if(user.isModified('password')){
        user.password = await bcrypt.hash(user.password, 8)
    }

    next()
})

userSchema.methods.generateAuthToken = async function(){
    const user = this

    const token = jwt.sign({_id:user._id.toString()}, process.env.JWT_SECRET)
    user.tokens = user.tokens.concat({token})
    await user.save()

    return token
}

userSchema.statics.findUserByCredential = async({email, password}) => {

    const user = await UserModel.findOne({email});

    if(!user){
        throw new Error('unable to login')
    }

    const isMatch = await bcrypt.compare(password, user.password);

    if(!isMatch){
        throw new Error('unable to login')
    }

    return user;

}

const UserModel = mongoose.model('user', userSchema)

module.exports = UserModel