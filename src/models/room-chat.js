const mongoose = require('mongoose')

const RoomChatSchema = new mongoose.Schema({
    'text':{
        type: String,
        trim: true        
    },
    'location':{
        type: String,
        trim: true        
    },
    'user':{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'user'
    },
    'room':{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'room'
    }
},{
    timestamps: true
})

const RoomChat = mongoose.model('room_chat', RoomChatSchema)

module.exports = RoomChat