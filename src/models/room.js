const mongoose = require('mongoose')

const RoomSchema = new mongoose.Schema({
    room_name:{
        type: String,
        required: [true, 'Room name is required'],
        trim: true,
        minlength:[4, 'Min 4 character required'],
    },
    is_public:{
        type: Boolean,
        default: false
    },
    host_id:{
        type: mongoose.Schema.Types.ObjectId,
        required:[true, 'Host is required']
    },
    chats:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'room_chat'
    }]
},{
    timestamps: true
})

RoomSchema.methods.toJSON = function(){
    const room = this
    const roomObj = room.toObject()

    delete roomObj.chats

    return roomObj
}

const RoomModel = mongoose.model('room', RoomSchema)

module.exports = RoomModel