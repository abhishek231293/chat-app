const path = require('path')
const http = require('http')
const bodyParser = require('body-parser');
const express = require('express')
const socketio = require('socket.io')
const socketServer = require('./socket-server')
const oneToOneChatServer = require('./one-to-chat-server')

require('./db/mongoose')
const userRouter = require('./routes/users')
const chatRouter = require('./routes/chat')

const app = express();
const server = http.createServer(app)
const io = socketio(server)
socketServer.ioConnection(io);
oneToOneChatServer.oneToOneIoConnection(io);

const port = process.env.port || 8000
const viewDirectoryPath = path.join(__dirname, './view/')
const publicDirectoryPath = path.join(__dirname, '../public/');

app.use(express.json())
app.use(bodyParser.json({ limit: '5mb' }));
app.use(bodyParser.urlencoded({ limit: '5mb', extended: true }));

app.use(userRouter)
app.use(chatRouter)

app.use('view',express.static(viewDirectoryPath))
app.use('/assets', express.static(publicDirectoryPath))

server.listen(port, ()=>{
    console.log(`App served at port ${port}!`)
    console.log(`http://localhost:${port}/`)
})