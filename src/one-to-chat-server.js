const oneToOneIoConnection = (io) => {

    var onlineUsers = [];

    // Register events on socket connection
    io.on('connection', function (socket) {

        console.log('New One:One User Connection Established')

        // Listen to newUser event sent by client and emit a newUser to the client with new list of online users
        socket.on('newUser', function (user) {
            var newUser = { id: socket.id, user: user };
            onlineUsers.push(newUser);

            io.to(socket.id).emit('newUser', newUser);
            io.emit('onlineUsers', onlineUsers);
        });

        // Listen to chantMessage event sent by client and emit a chatMessage to the client
        socket.on('chatMessage', function (message) {
            io.to(message.receiver).emit('chatMessage', message);
        });
  
        // Listen to notifyTyping event sent by client and emit a notifyTyping to the client
        socket.on('notifyTyping', function (sender, receiver) {
            io.to(receiver.id).emit('notifyTyping', sender, receiver);
        });

        // Listen to notifyStopTyping event sent by client and emit a notifyStopTyping to the client
        socket.on('notifyStopTyping', function (sender, receiver) {
            io.to(receiver.id).emit('notifyStopTyping', sender, receiver);
        });
  
        // Listen to disconnect event sent by client and emit userIsDisconnected and onlineUsers (with new list of online users) to the client 
        socket.on('disconnect', function () {
            onlineUsers.forEach(function (user, index) {
                if (user.id === socket.id) {
                    onlineUsers.splice(index, 1);
                    io.emit('userIsDisconnected', socket.id);
                    io.emit('onlineUsers', onlineUsers);
                }
            });
        });
    });
}

module.exports = {
    oneToOneIoConnection
};