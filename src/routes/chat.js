const express = require('express')
const auth = require("../middlewares/auth");
const multer = require('multer');
const ChatController = require('../controllers/chatController')

const router = new express.Router()
const upload = multer({ dest: 'public/img/attachments/', limits: { fieldSize: 5 * 1024 * 1024 } });

router.get('/room/:roomname', ChatController.viewRoomChat)
router.get('/dashboard', ChatController.viewDashboard)

router.get('/rooms', auth, ChatController.roomList)

router.post('/create-room', auth, ChatController.createRoom)
router.post('/save-room-chat', auth, ChatController.saveRoomChat)
router.get('/room-detail/:id', auth, ChatController.roomDetail)

router.post('/upload-attachment', auth, upload.single('avatar'), ChatController.uploadAttachment, (error, req, res, next)=>{

    return res.status(400).send({
        message:'Fail to send this attachment',
        errors: error,
        status: 400
    })

})

module.exports = router