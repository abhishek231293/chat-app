const express = require('express')
const auth = require("../middlewares/auth");
const AuthController = require('../controllers/authController')

const router = new express.Router()

router.get('/', AuthController.viewLogin)
router.get('/sign-up', AuthController.viewRegister)

router.post('/user', AuthController.registerUser)
router.post('/user/login', AuthController.loginUser)
router.post('/user/logout', auth, AuthController.logoutUser)


// router.route('/test')
//     .get( AuthController.test)
//     .post( AuthController.test)

module.exports = router