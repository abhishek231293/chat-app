const path = require('path')
const fs = require('fs');
const RoomModel = require('../models/room')
const RoomChatModel = require('../models/room-chat')
const UserModel = require('../models/user')


class ChatController {
    
    static viewRoomChat = async (req, res) => {
        const loginPath = path.join(__dirname, '../view/room.html')
        return res.sendFile(loginPath);
    }

    static viewDashboard = async (req, res) => {
        const loginPath = path.join(__dirname, '../view/dashboard.html')
        return res.sendFile(loginPath);
    }

    static createRoom = async (req, res) => {
        try{

            const roomField = {
                ...req.body,
                host_id: req.user._id
            }
            const room  = new RoomModel(roomField)

            await room.save()

            return res.status(200).send({
                
                status: 200,
                message: 'Room Successfully Created',
                data: {
                        roomData:room
                      }
               
            })

        }catch(error){
            console.log(error);
            return res.status(400).send({
                message:error,
                status: 400
            })
        }
    }

    static roomList = async (req, res) => {
        try{

            console.log(req.user._id.toString());

            const data = await RoomModel.aggregate([
                                                    { $match: { 
                                                        $or: [ 
                                                            { host_id: req.user._id },  
                                                            {
                                                                $and:[{host_id : { $ne: req.user._id  }, is_public:true }] 
                                                            }]
                                                        }
                                                    },
                                                    {
                                                        $lookup: {
                                                            from: "users", // collection name in db
                                                            localField: "host_id",
                                                            foreignField: "_id",
                                                            as: "host"
                                                        }
                                                    },
                                                ]).sort({ createdAt: -1 })
                                                

            res.status(200).send({
                status: 200,
                message: 'Room Fetched Successfully' ,
                data     
            })

        }catch(error){
            console.log(error)
            return res.status(400).send({
                message:'Unable to fetch rooms',
                errors: error,
                status: 400
            })
        }
    }

    static saveRoomChat = async (req, res) => {

        try{

            let data;
            if(req.body.text){
                data = {
                    text: req.body.text
                }
            }else{
                data = {
                    location: req.body.location
                }
            }

            const roomchat = new RoomChatModel(data)

            const room = await RoomModel.findById(req.body.room_id)
            roomchat.room = room
            
            const user = await UserModel.findById(req.user._id)
            roomchat.user = user
            
            await roomchat.save()

            room.chats.push(roomchat)
            await room.save()

            res.status(200).send({
                status: 200,
                message: 'Message Successfully' ,
                data:roomchat     
            })

        }catch(error){
            console.log(error)
            return res.status(400).send({
                message:'Unable to fetch rooms',
                errors: error,
                status: 400
            })
        }

    }

    static roomDetail = async (req, res)=>{

        try{
            const _id = req.params.id;

            // const data = await RoomModel.findOne({_id}).populate('chats')

            const data = await RoomChatModel.find({room:_id}).populate([{path:'user'},{path:'room'}])
           
            if(!data){
                return res.status(200).send({
                    status: 404,
                    message: 'No room found'   
                })
            }
            res.status(200).send({
                status: 200,
                message: 'Room fetched Successfully' ,
                data
            })


        } catch(error){
            console.log(error)
            return res.status(400).send({
                message:'Unable to fetch rooms',
                errors: error,
                status: 400
            })
        }

    }

    static uploadAttachment = async (req, res) => {
        try{

        var fileBase64Data = req.body.fileToUpload;
        var fileName = req.body.fileName;
        var filePath = '/img/attachments/' + new Date().getTime() + fileName.split(' ').join('_');

        fs.writeFile('public'+filePath, fileBase64Data, 'base64', function (error) {
            if (error) {
                return res.status(500).send({
                    message:'Unable to send attachment',
                    errors: error,
                    status: 500
                })
            }

            res.status(200).send({
                status: 200,
                message: 'Attachment Sent Successfully' ,
                data:{ filePath: 'assets'+filePath }
            })
        });

        } catch(error){
            console.log(error)
            return res.status(400).send({
                message:'Unable to send attachment',
                errors: error,
                status: 400
            })
        }
    }

}

module.exports = ChatController