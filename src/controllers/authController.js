const UserModel = require('../models/user')
const auth = require('../middlewares/auth')
const path = require('path')

class AuthController {

    static viewLogin = async (req, res) => {
        const loginPath = path.join(__dirname, '../view/login.html')
        return res.sendFile(loginPath);
    }

    static viewRegister = async (req, res) => {
        const loginPath = path.join(__dirname, '../view/signup.html')
        return res.sendFile(loginPath);
    }

    static registerUser = async (req, res) => {

        try{
    
            const user = new UserModel(req.body)
    
            await user.save()
            const token = await user.generateAuthToken()
    
            return res.status(200).send(
                                            {
                                            
                                            status: 200,
                                            message: 'Successfully Registered',
                                            data: {
                                                    userData:user,
                                                    token 
                                                }
                                        
                                            }
                                        )
    
        }catch(error){
           
            let message = 'Email id already exist'
    
            for(const err in error.errors){
                message = error.errors[err].message
            }
            
            return res.status(400).send({
                message,
                status: 400,
                data:[]
            })
        }
    
    }

    //call b kro
    

    static loginUser = async (req, res) => {

        try{
            
            const user = await UserModel.findUserByCredential(req.body);
            console.log(user);
            const token = await user.generateAuthToken();
            return res.status(200).send({
                status: 200,
                message: 'Login Successfully!',
                data: {
                    userData:user, token 
                }
            })
        }catch(error){
           
            return res.status(400).send({
                message:'Invalid credentials!',
                status: 400
            })
        }
    
    }

    static logoutUser = async (req, res) => {

        try{

            req.user.tokens = req.user.tokens.filter((token)=> req.token !== token.token)
    
            await req.user.save()
    
            return res.status(200).send({
                status: 200,
                message: 'Logout Successfully!'
            })
        }catch(error){
            return res.status(400).send({
                message:'Something went wrong!',
                status: 400
            })
        }

    }

}

module.exports = AuthController