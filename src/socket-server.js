const Filter = require('bad-words')
const userUtils = require('./utils/users')
const { generateMessage } = require('./utils/messages');

const User = new userUtils()

const ioConnection = (io) => {
    io.on('connection', (socket) => {
        console.log('New connection established!')
    
        //socket.emit,  socket.broadcast.emit,  io.emit
    
        socket.on('join', ({userid, username, room, roomname}, callback)=>{
            const response = User.addUser({id:socket.id, userid, username, room})
    
            if(response.status != 200){
                return callback(response)
            }
    
            socket.join(response.data.room)

            //Welcome Message
            // socket.emit('message', generateMessage('Admin', `Welcome ${response.data.username.toUpperCase()}`)) //This emit to you only
            socket.broadcast.to(response.data.room).emit('notify', generateMessage('Admin', `${response.data.username} has joined the room!`)) //This emit to everyone excluding you    
    
            io.to(response.data.room).emit('roomData', {
                room: response.data.room,
                roomname,
                users: User.getUsersInRoom(response.data.room)
            })
    
            callback(response)
        })

        socket.on('typing', (data) => {
            const response = User.getUser(socket.id)
    
            if(response.status !== 200){
                return callback({     
                    status: response.status,
                    message: response.message
                })
                
            }
            socket.broadcast.to(response.data.room).emit('typing', data)
        });

        socket.on('stoptyping', (data) => {
            const response = User.getUser(socket.id)
    
            if(response.status !== 200){
                return callback({     
                    status: response.status,
                    message: response.message
                })
                
            }
            socket.broadcast.to(response.data.room).emit('stoptyping', data)
        });
    
        socket.on('sendMessage', (message, callback)=>{
            const filter = new Filter()
    
            if(filter.isProfane(message)){
                return callback({     
                    status: 200,
                    message: 'Profanity is not allowed'
                })
            }
    
            const response = User.getUser(socket.id)
    
            if(response.status !== 200){
                return callback({     
                    status: response.status,
                    message: response.message
                })
                
            }
    
            io.to(response.data.room).emit('message',generateMessage(response.data.username, message)) //This emit to every connection including you also
            callback({     
                status: 200,
                message: 'Message delivered',
                data:{
                    room_id: response.data.room,
                    message
                }
                
            })
        })
    
        socket.on('shareLocation', (location, callback)=>{
    
            const response = User.getUser(socket.id)
    
            if(response.status !== 200){
                return callback({     
                    status: response.status,
                    message: response.message
                })
                
            }
    
            const latitude = location.latitude
            const longitude = location.longitude
    
            io.to(response.data.room).emit('locationMessage', generateMessage(response.data.username, `https://google.com/maps?q=${latitude},${longitude}`))
            callback({     
                status: 200,
                message: 'Location delivered',
                data:{
                    room_id: response.data.room,
                    location: `https://google.com/maps?q=${latitude},${longitude}`
                }
                
            })
        })
    
        socket.on('disconnect', ()=>{
            const response = User.removeUser(socket.id)
    
            if(response.status === 200){
                io.to(response.data.room).emit('notify', generateMessage(response.data.username, `${response.data.username} has left the chat room`))
    
                io.to(response.data.room).emit('roomData', {
                    room: response.data.room,
                    users: User.getUsersInRoom(response.data.room)
                })
            }
        })
    })
}   

module.exports = {
    ioConnection
};