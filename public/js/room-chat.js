window.isTabActive = true;
let newMessage = 0;

window.onfocus = function () { 
    window.isTabActive = true; 
    setTimeout(() => {
        document.title = `Chat App`        
    }, 5000);
}; 

window.onblur = function () { 
    window.isTabActive = false; 
}; 

// test
setInterval(function () { 
  if(window.isTabActive){
    newMessage = 0;
  }
}, 1000);

function autoScroll(noForce = false){

    if(!noForce){
        // New message element
        const $newMessage = $messages.lastElementChild

        // Height of the new message
        const newMessageStyles = getComputedStyle($newMessage)
        const newMessageMargin = parseInt(newMessageStyles.marginBottom)
        const newMessageHeight = $newMessage.offsetHeight + newMessageMargin

        // Visible height
        const visibleHeight = $messages.offsetHeight

        // Height of messages container
        const containerHeight = $messages.scrollHeight

        // How far have I scrolled?
        const scrollOffset = $messages.scrollTop + visibleHeight

        if (containerHeight - newMessageHeight <= scrollOffset) {
            $messages.scrollTop = $messages.scrollHeight
        }
    }else{
        $messages.scrollTop = $messages.scrollHeight
    }
    

   
}

const socket = io()

//Elements
const locationQuery = document.querySelector('#share-location');
const sendQuery = document.querySelector('#send')
const messageQuery = document.querySelector('#message')
const sidebarQuery = document.querySelector('#sidebar')
const $messages = document.querySelector('#messages')

//Template
const templateMessageQuery = document.querySelector('#template-message')
const allMessagesQuery = document.querySelector('#messages')
const sidebarTemplateQuery = document.querySelector('#sidebar-template')

// Option
// const {username, room} = Qs.parse(location.search, {ignoreQueryPrefix:true})

const pageURL = window.location.href;
const room = pageURL.substr(pageURL.lastIndexOf('/') + 1);
const localData = localStorage.getItem('chat-app-login-user')
let username;
let userid;
let roomname;

const getToken = () => {
    const localData = localStorage.getItem('chat-app-login-user')

    if(!localData){
        return false
    }

    const data = JSON.parse(localData)
    return data.token
}

const getRoomDetail = async () =>{
    const token = getToken()
    const response = await getRequestCreatorWithToken('/room-detail/'+room, token)

    if(response.status !== 200){
        window.location.href = '/dashboard'            
    }

    return response

}

// Function to play a audio when new message arrives on selected chatbox
const playNewMessageAudio = () => {
    (new Audio('https://notificationsounds.com/soundfiles/8b16ebc056e613024c057be590b542eb/file-sounds-1113-unconvinced.mp3')).play();
  }
  
// Function to play a audio when new message arrives on selected chatbox
const playNewMessageNotificationAudio = () => {
    (new Audio('https://notificationsounds.com/soundfiles/dd458505749b2941217ddd59394240e8/file-sounds-1111-to-the-point.mp3')).play();
}

const RoomChat = async () => {
    if(!localData){
        window.location.href = '/'
   }else{
       
       const userData = JSON.parse(localData)
       username = userData.userData.name
       userid = userData.userData._id

   
       const roomResponse = await getRoomDetail();

       if(roomResponse.status !== 200){
            alert(`Error: ${roomResponse.message}`)
            location.href = '/dashboard'
            return
        }
        
        let defaultChatHtml = '';
        if(roomResponse.data){
            const chats = roomResponse.data

            chats.forEach((chat, key)=>{

                if(!key){
                    roomname = chat.room.room_name
                }

                let me = false
                if( username.toLowerCase() == chat.user.name.toLowerCase()){
                    me = true;
                    chat.user.name = 'You'
                    user_profile =  '/assets/user_profile/user.png'
                }else{
                    user_profile =  '/assets/user_profile/user2.png'
                }
                
                if(chat.text){
                    defaultChatHtml += Mustache.render(templateMessageQuery.innerHTML, {
                        message: chat.text,
                        username: chat.user.name,
                        createdAt: moment.utc(chat.createdAt).fromNow(),
                        user_profile,
                        me
                    })
                }

                if(chat.location){
                    defaultChatHtml += Mustache.render(templateMessageQuery.innerHTML, {
                        url: chat.location,
                        username: chat.user.name,
                        createdAt: moment.utc(chat.createdAt).fromNow(),
                        user_profile,
                        me
                    })
                }
                
            })
        }

        socket.emit('join', {userid, username, room, roomname}, (response)=>{
            if(response.status !== 200){
                alert(`Error: ${response.message}`)
                location.href = '/dashboard'
                return
            }

            if(defaultChatHtml){
                allMessagesQuery.insertAdjacentHTML('beforeend', defaultChatHtml)
                autoScroll(true)
            }
            
        })

        socket.on('typing', function (data) {
            const typingQuery = document.querySelector('#typing')
            typingQuery.innerHTML = `${data.from} is typing`
            typingQuery.setAttribute("style", "display: block;");
            autoScroll()
        });

        socket.on('stoptyping', function (data) {
            const typingQuery = document.querySelector('#typing')
            typingQuery.innerHTML = ``
            typingQuery.setAttribute("style", "display: none;");
            autoScroll()
        });

        $("#message").keyup(function (event) {
            var data = $("#message").val();

            if (data == null || data == "") {
                socket.emit('stoptyping', {
                    from: username.toLowerCase()
                });
                return;
            }

            socket.emit('typing', {
                from: username.toLowerCase()
            });
        })

        $("#message").onblur = function() {
            socket.emit('stoptyping', {
                from: username.toLowerCase()
            });
        };

        socket.on('notify', (notification)=>{

            const html = Mustache.render(templateMessageQuery.innerHTML, {
                notification:notification.text.toUpperCase()
            })  

            allMessagesQuery.insertAdjacentHTML('beforeend', html)
            autoScroll()

        })

        socket.on('message', (message)=>{

            let me = false
            if( username.toLowerCase() == message.username){
                me = true;
                message.username = 'You'
                user_profile =  '/assets/user_profile/user.png'
            }else{
                user_profile =  '/assets/user_profile/user2.png'
            }
            const html = Mustache.render(templateMessageQuery.innerHTML, {
                message: message.text,
                username: message.username,
                createdAt:  moment.utc(message.createdAt).fromNow(),
                user_profile,
                me
            })

            allMessagesQuery.insertAdjacentHTML('beforeend', html)

            if(!window.isTabActive){
                newMessage++
                document.title = `(${newMessage}) Chat App`
                if( message.username != 'You' ){
                    playNewMessageNotificationAudio()
                }
            }else{
                if( message.username != 'You' ){
                    playNewMessageAudio()
                }
                autoScroll()
            }

        })

        socket.on('locationMessage', (location)=>{

            let me = false
            if( username.toLowerCase() == location.username){
                me = true;
                location.username = 'You'
                user_profile =  '/assets/user_profile/user.png'
            }else{
                user_profile =  '/assets/user_profile/user2.png'
            }
            const html = Mustache.render(templateMessageQuery.innerHTML, {
                url: location.text,
                username: location.username,
                createdAt: moment.utc(location.createdAt).fromNow(),
                user_profile,
                me
            })

            allMessagesQuery.insertAdjacentHTML('beforeend', html)
            
            if(!window.isTabActive){
                newMessage++
                document.title = `(${newMessage}) Chat App`
                if( message.username != 'You' ){
                    playNewMessageNotificationAudio()
                }
            }else{
                if( message.username != 'You' ){
                    playNewMessageAudio()
                }
                autoScroll()
            }

        })

        socket.on('roomData', ({room, users, roomname})=>{
            
            if(users.status === 200){
                const html = Mustache.render(sidebarTemplateQuery.innerHTML, {
                    room,
                    roomname,
                    users: users.data
                })
            
                sidebarQuery.innerHTML = html
            }

        })

        sendQuery.addEventListener('click', (e)=>{

            e.preventDefault()
            const message = messageQuery.value

            if(!message){
                alert('Enter message')
                return
            }

            sendQuery.setAttribute('disabled', 'disabled')

            socket.emit('sendMessage', message, async (response)=>{
                sendQuery.removeAttribute('disabled')
                messageQuery.focus()
                
                if(response.status == 200){
                    const token = getToken()
                    formData = {
                        room_id: response.data.room_id,
                        text: response.data.message
                    }
                    const request = postRequestCreatorWithToken('/save-room-chat', formData, token)
                    
                }

                
            })
            document.querySelector('#message').value = ''
        })


        locationQuery.addEventListener('click', ()=>{

            if(!navigator.geolocation){
                return alert('Geolocation is not supported by your browser');
            }

            locationQuery.setAttribute('disabled', 'disabled')

            navigator.geolocation.getCurrentPosition((position)=>{

                locationQuery.removeAttribute('disabled');

                const location = { 
                                    'latitude':position.coords.latitude, 
                                    'longitude':position.coords.longitude 
                                }

                socket.emit('shareLocation', location, async (response)=>{

                    if(response.status == 200){
                        const token = getToken()
                        formData = {
                            room_id: response.data.room_id,
                            location: response.data.location
                        }
                        const request = await postRequestCreatorWithToken('/save-room-chat', formData, token)
                        
                    }
    
                    console.log(response.message)

                })

            })

        })

        messageQuery.focus()
       
   
   }
}

RoomChat()