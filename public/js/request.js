const postRequestCreatorWithToken = async (url = '', data = {}, token) => {
    const response = await fetch(url, {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json',
        'Authorization' : 'Bearer '+token
      },
      body: JSON.stringify(data) 
    });
    return response.json();
}

const postRequestCreatorNoToken = async (url = '', data = {}) => {
    const response = await fetch(url, {
      method: 'POST',
      mode: 'cors',
      cache: 'no-cache',
      credentials: 'same-origin',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data) 
    });
    return response.json();
}

const getRequestCreatorWithToken = async (url = '', token) => {
  const response = await fetch(url, {
    method: 'GET',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      'Authorization' : 'Bearer '+token
    }
  });
  return response.json();
}

const uploadFileRequestCreator = async (url = '', data, token) => {
  const response = await fetch(url, {
    method: 'POST',
    mode: 'cors',
    cache: 'no-cache',
    credentials: 'same-origin',
    headers: {
      'Authorization' : 'Bearer '+token
    },
    body:data
  });
  return response.json();
}