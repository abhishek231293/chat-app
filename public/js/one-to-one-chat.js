const socket = io()

window.isTabActive = true;
let newMessage = 0;
let allChatMessages = [];
let chatNotificationCount = [];
let allNewMessageCount = 0;
let allNewNotificationCount = 0;
let me = {};
let myFriend = {};

// Document Ready function called automatically on page load
$(document).ready( function () {

    getUserDetail(async (error, response)=>{
        if(error){
            alert(error)
            location.href = '/'
        }
        await socket.emit('newUser', {...userDetail.userData})

        // Function call to initilize file uploader
        initializeFileUploader();

        // Function will be called when file is selected
        fileIsSelected();

    })

});

// Function to initialize file uploader
function initializeFileUploader() {
    $("#imgAttachment").click(function () {
        $("#fileAttachment").click();
    });
}
  
// Function to be called when file is selected in file uploader
function fileIsSelected() {
    $('#fileAttachment').on('change', function () {
        var file = this.files[0];
        if (file.size > 5 * 1024 * 1024) {
            alert('max upload size is 5 MB');
        } else {
            var reader = new FileReader();
            reader.addEventListener("load", function () {
                uploadAttachment(file.name, reader.result.split(',')[1]);
            }, false);
            if (file) {
                reader.readAsDataURL(file);
            }
        }
    });
}

// Function to hadle ajax file upload
async function uploadAttachment (fileName, fileData) {
    var message = {};
    var formData = new FormData();
    formData.append('fileName', fileName);
    formData.append("fileToUpload", fileData);
  
    const response = await uploadFileRequestCreator('/upload-attachment', formData, userDetail.token)

    if(response.status != 200){
        return alert(response.message)
    }
    
    message.type = 'file';
    message.text = response.data.filePath;
    message.sender = me.id;
    message.receiver = myFriend.id;
    appendSendersChatboxMessage(message);

}

// function to emit an even to notify friend that I am typing a message 
function notifyTyping() {
    socket.emit('notifyTyping', me, myFriend);
}

// function to emit an even to notify friend that I am typing a message 
function notifyStopTyping() {
    socket.emit('notifyStopTyping', me, myFriend);
}
 
// Load all messages for the selected user
function loadChatBox(messages) {
    $('#form').show();
    $('#messages').html('').show();
    messages.forEach(function (message) {
      var cssClass = (message.sender == me.id) ? 'right' : 'left';
      // Function call to append receiver's chat message to chatBox
      appendMessage(message, cssClass);
    });
    chatboxScrollBottom();
}

// Function to update chat notifocation count
function updateChatNotificationCount(userId) {
    var count = (chatNotificationCount[userId] == undefined) ? 1 : chatNotificationCount[userId] + 1;
    allNewMessageCount++;
    chatNotificationCount[userId] = count;
    allNewNotificationCount = 1
    $('#notification-message').html(`${allNewMessageCount} new messages`)
    $('#global-notification').html(allNewNotificationCount)
    $('#global-message-count').html(allNewMessageCount)
    
    $("#bell-ring").addClass("bell-ring");
    $("#global-message-count").addClass("bell-ring");

    $('#' + userId + ' span.chatNotificationCount').html(count);
    $('#' + userId + ' span.chatNotificationCount').show();
}

// Function to update chat notifocation count
function updateActiveNewMessageCount(userId) {
    $('#newMessageNotification').show()
    var count = (chatNotificationCount[userId] == undefined) ? 1 : chatNotificationCount[userId]
    $('#newMessageNotification').title = `${count} New Messages`
    $('#newMessageNotification').html(`${count} New Messages`);
    $('#newMessageNotification').show();

}
  
// Function to clear chat notifocation count to 0
function clearChatNotificationCount(userId) {

    allNewMessageCount = (allNewMessageCount - (chatNotificationCount[userId]?chatNotificationCount[userId]:0))
    $('#global-message-count').html(allNewMessageCount)
    allNewNotificationCount = allNewMessageCount?1:0

    if(allNewMessageCount){
        $("#bell-ring").addClass("bell-ring");
        $("#global-message-count").addClass("bell-ring");
    }else{
        $("#bell-ring").removeClass("bell-ring");
        $("#global-message-count").removeClass("bell-ring");
    }
    $('#global-notification').html(allNewNotificationCount)
    $('#notification-message').html(`${allNewMessageCount} new messages`)
    chatNotificationCount[userId] = 0;
    $('#' + userId + ' span.chatNotificationCount').hide();
}

// Function to be called when a friend is selected from the list of online users
function selectUerChatBox(element, userId, userName) {


      // Logic to set window height
    const windowHeight = $(window).height();
    const cardHeigh = $('#card-stats').height();

    
    const chatBoxHeigh = windowHeight - cardHeigh;

    console.log(windowHeight, cardHeigh, chatBoxHeigh)
    $('.direct-chat-messages').css('min-height', chatBoxHeigh).css('max-height', chatBoxHeigh);


    myFriend.id = userId;
    myFriend.name = userName;

    $('#activeUserChatName').html(myFriend.name)
  
    $('#form').show();
    $('#messages').show();
    $('#userChatBox').show()
    
    $('#onlineUsers li').removeClass('active');
    $(element).addClass('active');

    $('#notifyTyping').text('');
    $('#txtChatMessage').val('').focus();
  
    // Reset chat message count to 0
    clearChatNotificationCount(userId);
  
    // load all chat message for selected user 
    if (allChatMessages[userId] != undefined) {
      loadChatBox(allChatMessages[userId]);
    } else {
      $('#messages').html('');
    }
}

// Function to be called when sent a message from chatbox
function submitfunction() {
    var message = {};
    text = $('#txtChatMessage').val();
   
    if (text != '') {
      message.type = 'text';
      message.text = text;
      message.sender = me.id;
      message.receiver = myFriend.id;
      // Function call to send attached file to sender/seceiver in chatbox
      appendSendersChatboxMessage(message);
    }
  
    $('#txtChatMessage').val('').focus();
}

// Function to append a chat message to the senders chatBox
function appendSendersChatboxMessage(message) {
    // Function call to append senders chat message to chatBox
    appendMessage(message, 'right');
  
    if (allChatMessages[myFriend.id] != undefined) {
      allChatMessages[myFriend.id].push(message);
    } else {
      allChatMessages[myFriend.id] = new Array(message);
    }
    socket.emit('chatMessage', message);
}

// Append a single chant message to the receivers chatbox
function appendReceiversChatMessage(message) {
    if (message.receiver == me.id && message.sender == myFriend.id) {
      var chatSide = (message.sender == me.id) ? 'right' : 'left';
  
      // Function call to append receivers chat message to chatBox
      appendMessage(message, chatSide);
  
      if(!window.isTabActive){
        playNewMessageNotificationAudio();
        updateChatNotificationCount(message.sender);
        updateActiveNewMessageCount(message.sender);
      }else{
        // Function call to play notification sound
        playNewMessageAudio();
      }
    } else {
      playNewMessageNotificationAudio();
      updateChatNotificationCount(message.sender);
    }
  
    if (allChatMessages[message.sender] != undefined) {
      allChatMessages[message.sender].push(message);
    } else {
      allChatMessages[message.sender] = new Array(message);
    }
}

// Function to append chat application to senders/receivers chatBox
function appendMessage(message, chatSide) {
    var htmlMessage = '';
    var fileTypeRE = /\.(jpe?g|png|gif|bmp)$/i;
  
    var messageTimestamp = new Date().toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true });
  
    if (message.type === 'text') {
      
        htmlMessage = ` <div class="direct-chat-msg ${chatSide}">
                            <img class="direct-chat-img" src="/assets/user_profile/user2.png" alt="message user image">
                            <div class="direct-chat-text">
                                ${message.text}
                                <span class="direct-chat-timestamp timestamp-${chatSide} float-right">${messageTimestamp}</span>
                            </div>
                        </div>`;

    } else if (message.type === 'file') {

      if (fileTypeRE.test(message.text)) {
        
        htmlMessage = ` <div class="direct-chat-msg ${chatSide}">
                            
                            <img class="direct-chat-img" src="/assets/user_profile/user2.png" alt="message user image">
                            <div style="width:50%" class="direct-chat-text">
                                <img style="width:100%; height:auto;" class="chatImage" src="${message.text}" alt="Attachement" />
                                <span class="direct-chat-timestamp timestamp-${chatSide} float-right">${messageTimestamp}</span>
                            </div>
                        </div>`;
                        
        // htmlMessage = '<li class="' + chatSide + '"><img class="chatImage" src="' + message.text + '" alt="" />' + messageTimestamp + '</li>';

      } else {

        htmlMessage = ` <div class="direct-chat-msg ${chatSide}">
                           
                            <img class="direct-chat-img" src="/assets/user_profile/user2.png" alt="message user image">
                            <div style="width:20%" class="direct-chat-text">
                            <a href="${message.text}">
                                <img class="chatImage" src="/assets/img/if_Download_1031520.png" alt="Attachement" />
                                <span class="direct-chat-timestamp timestamp-${chatSide} float-right">${messageTimestamp}</span>
                            </a>
                            </div>
                        </div>`;
                        
        // htmlMessage = '<li class="' + chatSide + '"><a href="' + message.text + '"><img class="chatImage" src="/assets/img/if_Download_1031520.png" alt="Attachement" /></a>' + chatMessageTimestamp + '</li>';

      }
    }
  
    $('#messages').append(htmlMessage);

    if(!window.isTabActive){
        
    }else{
        chatboxScrollBottom();
    }
}

function chatboxScrollBottom() {
    $('#messages').animate({ scrollTop: $('#messages').prop('scrollHeight') });
}

const playNewMessageAudio = () => {
    (new Audio('https://notificationsounds.com/soundfiles/8b16ebc056e613024c057be590b542eb/file-sounds-1113-unconvinced.mp3')).play();
}
  
const playNewMessageNotificationAudio = () => {
    (new Audio('https://notificationsounds.com/soundfiles/dd458505749b2941217ddd59394240e8/file-sounds-1111-to-the-point.mp3')).play();
}

// ############# Event listeners and emitters ###############

// Listen to newUser even to set client with the current user information saved
socket.on('newUser', function (response) {
    me = response;
});

// Listen to onlineUsers event to update the list of online users and count of online users
socket.on('onlineUsers', function (onlineUsers) {
    var usersList = '';
  
    onlineUsers.forEach(function (userDetail) {
       
      if (userDetail.id != me.id) {
  
        // if (onlineUsers.length == 2) {
        //     myFriend.id = userDetail.id;
        //     myFriend.name = userDetail.user.name;
        //       $('#form').show();
        //       $('#messages').show();
        // }

        usersList += `<li onclick="selectUerChatBox(this, '${userDetail.id}', '${userDetail.user.name}')" id="${userDetail.id}" style="margin-left:10px;" class="nav-item">
                        <a  href="javascript:void(0)" class="nav-link">
                            <p>
                                <i style="font-size:12px; color:#70E22F;" class="fa fa-circle"></i> <span style="margin-left:5px;"> ${userDetail.user.name}</span>
                                <span style="display:none" class="badge chatNotificationCount badge-danger right"> 0 </span>
                            </p>
                        </a>
                        
                    </li>`
      }
    });
    document.querySelector('#total-friends').innerHTML = (onlineUsers.length-1);
    $('#friends-list').html(usersList);
});

// Listen to notifyTyping event to notify that the friend id typing a message
socket.on('notifyTyping', function (sender, recipient) {
    if (myFriend.id == sender.id) {
      $('#notifyTyping').text(sender.user.name + ' is typing ...');
    }
});

// Listen to notifyStopTyping event to notify that the friend id has stoped typing a message
socket.on('notifyStopTyping', function (sender, recipient) {
    if (myFriend.id == sender.id) {
      $('#notifyTyping').text('');
    }
});

// Listen to chantMessage event to receive a message sent by my friend 
socket.on('chatMessage', function (message) {
    $('#notifyTyping').text('');
    // Function call to append a single chant message to the receivers chatbox
    appendReceiversChatMessage(message);
});
  
// Listen to userIsDisconnected event to remove its chat history from chatbox
socket.on('userIsDisconnected', function (userId) {
    delete allChatMessages[userId];
    if (userId == myFriend.id) {
    //   $('#form').hide();
    //   $('#messages').html('').hide();
    }
});

window.onfocus = function () { 
    window.isTabActive = true; 

    // $('#newMessageNotification').title = ''
    // $('#newMessageNotification').html('');
    // $('#newMessageNotification').show();
    $('#newMessageNotification').hide()
    clearChatNotificationCount(myFriend.id);
    setTimeout(() => {
        document.title = `Chat App`        
    }, 5000);
}; 

window.onblur = function () { 
    window.isTabActive = false; 
}; 