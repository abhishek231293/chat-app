const signUpButtonQuery = document.querySelector('#signup')
const signInButtonQuery = document.querySelector('#signin')

if(signUpButtonQuery){
    signUpButtonQuery.addEventListener('click', async (e)=>{

        e.preventDefault();
        const formData = validateSignUpForm();
        if(!formData){
            return alert('Invalid form fields')
        }
    
        const request = await postRequestCreatorNoToken('/user', formData)
    
        if(request.status != 200){
            return alert(request.message)
        }
    
        localStorage.setItem('chat-app-login-user', JSON.stringify(request.data))
        alert(request.message)
        window.location.href = 'dashboard'
    
    })
}

if(signInButtonQuery){
    signInButtonQuery.addEventListener('click', async (e)=>{

        e.preventDefault();
        const formData = validateSignInForm();
        if(!formData){
            return alert('Email Id & Password Required')
        }

        const request = await postRequestCreatorNoToken('/user/login', formData)

        if(request.status != 200){
            return alert(request.message)            
        }

        localStorage.setItem('chat-app-login-user', JSON.stringify(request.data))
        alert(request.message)
        window.location.href = 'dashboard'

    })
}
const validateSignUpForm = () => {
    const {name, email, password} = getSignUpFormValues();

    if(name.trim() && email.trim() && password.trim()){
        if(password.length > 7){
            return {name, email, password}
        }
    }

    return false
}

const validateSignInForm = () => {
    const { email, password } = getSignInFormValues();

    if(email.trim() && password.trim()){
        return {email, password}
    }

    return false
}

const getSignUpFormValues = () => {

    const name = document.querySelector('#name').value
    const email = document.querySelector('#email').value
    const password = document.querySelector('#password').value

    return {name, email, password}
}

const getSignInFormValues = () => {

    const email = document.querySelector('#email').value
    const password = document.querySelector('#password').value

    return { email, password }
}