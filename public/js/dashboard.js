const createRoomButtonQuery = document.querySelector('#createRoom')
const logoutButtonQuery = document.querySelector('#logout')
let userDetail;

if(createRoomButtonQuery){
    createRoomButtonQuery.addEventListener('click', async (e)=>{

        e.preventDefault()

        const token = getToken()

        if(!token){
            window.location.href = '/'
        }
    
        const formData = validateCreateRoomForm();
        if(!formData){
            alert('Invalid form fields')
        }
    
        const request = await postRequestCreatorWithToken('/create-room', formData, token)
    
        if(request.status == 400){
            return alert(request.message)
        }else if(request.status == 401){
            window.location.href = '/'
        }
        
        $("#create-room").modal("hide");
        alert(request.message)
        document.querySelector('#room-name').value = ''
        getRoomList()
    
    })
    
}

if(logoutButtonQuery){
    logoutButtonQuery.addEventListener('click', async (e)=>{

        e.preventDefault()

        const token = getToken()

        if(!token){
            window.location.href = '/'
        }
    
        const request = await postRequestCreatorWithToken('/user/logout', {}, token)
    
        if(request.status == 400){
            return alert(request.message)
        }else if(request.status == 401){
            window.location.href = '/'
        }
        
        alert(request.message)
        localStorage.removeItem('chat-app-login-user')
        window.location.href = '/'
    
    })
    
}

const validateCreateRoomForm = () => {
    const {room_name, is_public } = getRoomFormValues();

    if(room_name.trim()){
        return {room_name, is_public}
    }

    return false
}

const getRoomFormValues = () => {

    const room_name = document.querySelector('#room-name').value
    const is_public = document.querySelector('input[name="room_type"]:checked').value;

    return {room_name, is_public}
}

const getToken = () => {
    const localData = localStorage.getItem('chat-app-login-user')

    if(!localData){
        return false
    }

    const data = JSON.parse(localData)
    return data.token
}


const getUserDetail = (callback) => {
    const localData = localStorage.getItem('chat-app-login-user')

    if(!localData){
        callback('UnAuthorized', undefined)
    }

    userDetail = JSON.parse(localData)
    callback(undefined, userDetail)
}

const getRoomList = async () =>{
    const token = getToken()

    if(!token){
        window.location.href = '/'
    }

    const request = await getRequestCreatorWithToken('/rooms', token)

    if(request.status == 400){
        return alert(request.message)
    }else if(request.status == 401){
        window.location.href = '/'
    }

    const roomList = request.data

    let content = '';

    document.querySelector('#total-room').innerHTML = roomList.length;

    roomList.forEach((rooms)=>{
        content += `<li class="nav-item">
                        <a target="_blank" href="/room/${rooms._id}" class="nav-link">
                            ${rooms.is_public?'<i style=" color:green;" class="fa fa-globe nav-icon"></i>':'<i style="color:red;" class="fa fa-lock nav-icon"></i>'}

                            <p>
                                ${rooms.room_name} <br/> <small>Host - ${rooms.host[0].name}</small>
                            </p>
                        </a>
                    </li>`
    })

    const roomListQuery = document.querySelector('#room-list')
    roomListQuery.innerHTML = content

}

const renderDefaults = () => {
    document.querySelector('#username').innerHTML = userDetail.userData.name
}

document.addEventListener("DOMContentLoaded", function(event) { 
    getRoomList()
});

$(function() {
    getUserDetail((error, response)=>{
        if(error){
            alert(error)
            location.href = '/'
        }

        // console.log(response);
        renderDefaults()
    })
});